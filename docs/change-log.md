### 2.4.0
- 增加新的API：PearPlayer.isSupported()

### 2.3.10
- 在PearDownloader模块中添加自定义HTML标签功能，具体可参看PearDownloader.js
- 修复一些bug

### 2.3.9
- 修复下载完成后data channel没有关闭的bug
- 优化调度算法